+++
categories = ["all", "info"]
date = "2016-03-20T14:05:21-04:00"
tags = ["all", "info"]
title = "important dates"

+++

### Saturday, April 2nd
First practice & candy passed out  

### Tuesday, April 12th
Fish fry money & concession-duty buyout due

### Saturday, April 23th
Team picture day at the Kettering Rec Center.    
**Please be there by 8:45am!**  I will have jerseys there if I don't get them to you sooner. (I'm trying!)

### Monday, April 25th
Candy money due - turn in directly to the concession stand

