+++
categories = ["all", "info"]
date = "2016-03-20T14:06:57-04:00"
tags = ["all", "info"]
title = "Calendar"

+++


The season will consist of 6 practices in April followed by regular season games.  *All games and practices will take
 place at [State Farm Park](https://www.google.com/maps/place/State+Farm+Park/@39.708197,-84.111186,17z/data=!4m2!3m1!1s0x0:0xb064f19449d75e4b)*

## Practice Schedule
Key: &nbsp;&nbsp;<span class='scrimmage' style='width:20px'>&nbsp;&nbsp;&nbsp;&nbsp;</span> Scrimmage game - wear your
 hat!

<table>
<tr><th>Date</th><th>Day</th><th>Time</th><th>Field</th></tr>
<tr>
    <td>4/2</td>
    <td>Saturday</td>
    <td>4:00-5:00pm</td>
    <td>Diamond 5</td>
</tr>
<tr>
    <td>4/8</td>
    <td>Friday</td>
    <td>6:00-7:00pm</td>
    <td>Diamond 4</td>
</tr>
<tr>
    <td>4/10</td>
    <td>Sunday</td>
    <td>2:30-3:30pm</td>
    <td>Diamond 6</td>
</tr>
<tr>
    <td>4/13</td>
    <td>Wednesday</td>
    <td>6:00-7:00pm</td>
    <td>Diamond 6</td>
</tr>
<tr class="scrimmage">
    <td>4/17</td>
    <td>Sunday</td>
    <td>4:00-5:00pm</td>
    <td>Diamond 5</td>
</tr>
<tr>
    <td>4/20</td>
    <td>Wednesday</td>
    <td>6:00-7:00pm</td>
    <td>Diamond 4</td>
</tr>
<tr>
    <td>4/29</td>
    <td>Friday</td>
    <td>7:00-8:00pm</td>
    <td>Diamond 6</td>
</tr>
</table>

- - -

## Game Schedule

Key:  
<span class='team_home' style='width:20px'>&nbsp;&nbsp;&nbsp;&nbsp;</span> Home Team  
<span class='rainedout' style='width:20px'>&nbsp;&nbsp;&nbsp;&nbsp;</span> Rained Out  
<span class='makeup' style='width:20px'>&nbsp;&nbsp;&nbsp;&nbsp;</span> Make-up  

**All games will be 1 hour**  

<table>
<tr><th>Date</th><th>Day</th><th>Time</th><th>Field</th><th>With</th><th>Lineup</th><th>Snack</th></tr>
<tr class="team_home rainedout">
    <td>4/30</td>
    <td>Sat</td>
    <td>3:30-4:30</td>
    <td>Dia. 4</td>
    <td>Bees</td>
    <td><a href="/lineups/game_1.pdf">Lineup</a></td>
    <td>Sam K.</td>
</tr>
<tr class="rainedout">
    <td>5/1</td>
    <td>Sun</td>
    <td>3-4pm</td>
    <td>Dia. 4</td>
    <td>Grasshoppers</td>
    <td><a href="/lineups/game_2.pdf">Lineup</a></td>
    <td>Kaden S.</td>
</tr>
<tr class="team_home">
    <td>5/6</td>
    <td>Fri</td>
    <td>6-7pm</td>
    <td>Dia. 5</td>
    <td>Lugnuts</td>
    <td><a href="/lineups/game_3.pdf">Lineup</a></td>
    <td>Wyatt P.</td>
</tr>
<tr class="team_home">
    <td>5/7</td>
    <td>Sat</td>
    <td>5-6pm</td>
    <td>Dia. 5</td>
    <td>Thunder</td>
    <td><a href="/lineups/game_4.pdf">Lineup</a></td>
    <td>Danielle C.</td>
</tr>
<tr class="rainedout">
    <td>5/11</td>
    <td>Wed</td>
    <td>6-7pm</td>
    <td>Dia. 4</td>
    <td>Mudhens</td>
    <td><a href="/lineups/game_5.pdf">Lineup</a></td>
    <td>Caden M.</td>
</tr>
<tr class="rainedout">
    <td>5/14</td>
    <td>Sat</td>
    <td>5-6pm</td>
    <td>Dia. 5</td>
    <td>Bats</td>
    <td><a href="/lineups/game_6.pdf">Lineup</a></td>
    <td>Fielding F.</td>
</tr>
<tr class="team_home">
    <td>5/18</td>
    <td>Wed</td>
    <td>6-7pm</td>
    <td>Dia. 4</td>
    <td>Emeralds</td>
    <td><a href="/lineups/game_7.pdf">Lineup</a></td>
    <td>Landon P.</td>
</tr>
<tr class="rainedout">
    <td>5/20</td>
    <td>Fri</td>
    <td>6-7pm</td>
    <td>Dia. 5</td>
    <td>Hot Rods</td>
    <td><a href="/lineups/game_8.pdf">Lineup</a></td>
    <td>Hunter H.</td>
</tr>
<tr class="rainedout team_home">
    <td>5/21</td>
    <td>Sat</td>
    <td>4-5pm</td>
    <td>Dia. 6</td>
    <td>Mudcats</td>
    <td><a href="/lineups/game_9.pdf">Lineup</a></td>
    <td>Asher C.</td>
</tr>
<tr>
    <td>6/1</td>
    <td>Wed</td>
    <td>6-7pm</td>
    <td>Dia. 4</td>
    <td>River Dogs</td>
    <td><a href="/lineups/game_10.pdf">Lineup</a></td>
    <td>Carter R.</td>
</tr>
<tr>
    <td>6/4</td>
    <td>Sat</td>
    <td>4-5pm</td>
    <td>Dia. 6</td>
    <td>Bees</td>
    <td><a href="/lineups/game_11.pdf">Lineup</a></td>
    <td>Jackson R.</td>
</tr>
<tr class="team_home">
    <td>6/10</td>
    <td>Fri</td>
    <td>6-7pm</td>
    <td>Dia. 6</td>
    <td>Grasshoppers</td>
    <td><a href="/lineups/game_12.pdf">Lineup</a></td>
    <td>Claire C.</td>
</tr>
<tr class="team_home makeup">
    <td>6/12</td>
    <td>Sun</td>
    <td>3-4pm</td>
    <td>Dia. 4</td>
    <td>Bees</td>
    <td><a href="/lineups/game_1.pdf">Lineup</a></td>
    <td>Sam K.</td>
</tr>
<tr class="makeup">
    <td>6/16</td>
    <td>Thurs</td>
    <td>6-7pm</td>
    <td>Dia. 4</td>
    <td>Mudhens</td>
    <td><a href="/lineups/game_5.pdf">Lineup</a></td>
    <td>Caden M.</td>
</tr>
<tr class="makeup team_home">
    <td>6/23</td>
    <td>Thurs</td>
    <td>6-7pm</td>
    <td>Dia. 6</td>
    <td>Mudcats</td>
    <td><a href="/lineups/game_9.pdf">Lineup</a></td>
    <td>Asher C.</td>
</tr>
<tr class="makeup">
    <td>6/24</td>
    <td>Fri</td>
    <td>6-7pm</td>
    <td>Dia. 5</td>
    <td>Hot Rods</td>
    <td><a href="/lineups/game_8.pdf">Lineup</a></td>
    <td>Hunter H.</td>
</tr>
<tr class="makeup">
    <td>6/25</td>
    <td>Sat</td>
    <td>5-6pm</td>
    <td>Dia. 5</td>
    <td>Bats</td>
    <td><a href="/lineups/game_6.pdf">Lineup</a></td>
    <td>Fielding F.</td>
</tr>
<tr class="makeup">
    <td>6/26</td>
    <td>Sun</td>
    <td>3-4pm</td>
    <td>Dia. 4</td>
    <td>Grasshoppers</td>
    <td><a href="/lineups/game_2.pdf">Lineup</a></td>
    <td>Kaden S.</td>
</tr>
</table>

