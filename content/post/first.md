+++
date = "2016-03-20T12:51:56-04:00"
title = "Welcome Volcanoes!"

+++

## Welcome "Volcanoes" to the 2016 T-Ball Season

I'm excited to welcome all players and parents to the 2016 season of T-Ball at EKRC.  
  
My name is Chad but you can just call me Coach K since I'll be your head coach this year!    

I played baseball at EKRC for 12 years - from Pony T-Ball all the way through to the "Majors" so I know how much 
fun we can have learning to play this great sport!

## Important Information
There are a few things parents need to know:  

1.  **I need several willing parents to help out at practices and games.**  This is vital to keep your kids 
engaged and organized.  If you are willing to help out just come see me at the first practice and let me know.  You 
may want to bring a baseball glove as well. 
  
1.  **Concession Stand Duty** - As you should have been informed at registration each team is required to send 1 
volunteer per player to work in the concession stand for 1 shift.  This will be on a day that we don't have a game or
 practice.  This year EKRC is offering teams the opportunity to **buyout of this responsibility** for $100 / team.  
 There are currently 10 players on our team so that works out to $10 per player.  This will need to be a unanimous 
 decision we make as a team.  

1. **Rainouts / Cancellations** - The [EKRC website](http://ekrc.org) will have the latest information about rainouts
 or cancellations.  I will also try to reach all parents by phone and will post on this web page in the event of a 
 cancellation.  We hope to make up all rained out games!  
 
 
## Important Dates / Calendar
**First practice is April 2nd from 4-5pm on diamond 5**

Please see [Important Dates](/info/important-dates/) &amp; [Calendar](/info/calendar/) - also found at the top of 
every page!
