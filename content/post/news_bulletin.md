+++
date = "2016-04-09T21:17:04-04:00"
draft = false
title = "News Bulletin"

+++

### T-Ball Info  

I have uniforms and picture packets to hand out tomorrow.  I am going to be taking my son's shirt to 
have his name put on the back and if you want your child's name on their shirt you can leave it with me 
and tell me what name you want on there (first, last, nickname), the cost is $2.  
  
Fish fry ticket money is **due on 4/12** so try to get it to me tomorrow otherwise meet me at the park on 4/12 at 5:00.   
  
The game schedule is now up on the [Calendar](/info/calendar/)  
  
Lastly I am encouraging us to do the concession buyout for **$9 per player** rather than spend time doing concession 
duty.  It has to be a unanimous decision so **please let me know if you are against it**, otherwise I will assume we all
 agree.

